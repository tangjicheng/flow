package ops

import (
	"testing"
)

func TestChatGPT_Execute(t *testing.T) {
	op := &ChatGPT{}

	var inputText string = `{
    "model": "gpt-3.5-turbo",
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "Output a 10-word greeting"
      }
    ]
    }`

	inputs := TensorIO{"input_text": inputText}
	outputs := TensorIO{}

	err := op.Execute(inputs, outputs)

	if err != nil {
		t.Errorf("Execute ChatGPT failed with error: %v", err)
	}
	outputValue, ok := outputs["output_text"]
	if !ok {
		t.Errorf("Execute ChatGPT outputs do NOT have [output_text]")
	}
	outputText, ok := outputValue.(string)
	if !ok {
		t.Errorf("Execute ChatGPT output_text is NOT string type")
	}
	if len(outputText) <= 0 {
		t.Errorf("Execute ChatGPT output_text length <= 0")
	}
}

func TestChatGPT_GetInputsTemplate(t *testing.T) {
	op := &ChatGPT{}

	inputsTemplate := op.GetInputsTemplate()

	_, ok := inputsTemplate["input_text"]
	if !ok {
		t.Errorf("ChatGPT inputs do NOT have [input_text]")
	}
}

func TestChatGPT_GetOutputsTemplate(t *testing.T) {
	op := &ChatGPT{}

	outputsTemplate := op.GetOutputsTemplate()

	_, ok := outputsTemplate["output_text"]
	if !ok {
		t.Errorf("ChatGPT outputs do NOT have [output_text]")
	}
}
