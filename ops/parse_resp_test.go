package ops

import (
	"testing"
)

func TestParseResp_Execute(t *testing.T) {
	inputJSON := `{
	  "id": "chatcmpl-123",
	  "object": "chat.completion",
	  "created": 1677652288,
	  "model": "gpt-3.5-turbo-0613",
	  "choices": [{
	    "index": 0,
	    "message": {
	      "role": "assistant",
	      "content": "\n\nHello there, how may I assist you today?"
	    },
	    "finish_reason": "stop"
	  }],
	  "usage": {
	    "prompt_tokens": 9,
	    "completion_tokens": 12,
	    "total_tokens": 21
	  }
	}`

	expectedOutput := "\n\nHello there, how may I assist you today?"
	op := &ParseResp{}
	inputs := TensorIO{
		"input_text": inputJSON,
	}
	outputs := TensorIO{}
	err := op.Execute(inputs, outputs)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	outputText, ok := outputs["output_text"].(string)
	if !ok {
		t.Errorf("Expected 'output_text' to be a string")
	}

	if outputText != expectedOutput {
		t.Errorf("Expected output text to be '%s', got '%s'", expectedOutput, outputText)
	}
}
