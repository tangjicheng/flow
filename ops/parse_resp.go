package ops

import (
	"encoding/json"
	"errors"
	"flow/util"
)

type ParseResp struct {
}

func (op *ParseResp) Execute(inputs TensorIO, outputs TensorIO) error {
	responseStr, ok := inputs["input_text"].(string)
	if !ok {
		return errors.New("[ParseResp] inputs[input_text].(string) error")
	}
	chatgptResp := util.ChatCompletionResponse{}
	err := json.Unmarshal([]byte(responseStr), &chatgptResp)
	if err != nil {
		return err
	}

	// TODO
	// choose choice
	if len(chatgptResp.Choices) <= 0 {
		return errors.New("[ParseResp] len(chatgptResp.Choices) <= 0 error")
	}
	outputs["output_text"] = chatgptResp.Choices[0].Message.Content
	return nil
}

func (op *ParseResp) GetInputsTemplate() TensorIO {
	inputs := map[string]any{
		"input_text": "Here is the Raw ChatGPT API Response JSON Body",
		//"choice":     0, // TODO
	}
	return inputs
}

func (op *ParseResp) GetOutputsTemplate() TensorIO {
	outputs := map[string]any{
		"output_text": "Here is ChatGPT output content",
	}
	return outputs
}
