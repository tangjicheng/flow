package ops

import (
	"testing"
)

func TestSetChat_Execute(t *testing.T) {
	op := &SetChat{}

	inputs := TensorIO{
		"model":             "gpt-3.5-turbo",
		"temperature":       float32(0.0),
		"top_p":             float32(0.1),
		"frequency_penalty": float32(0.97),
		"system_prompt":     "Check if given text matches its given language code. If they match, then your response shall be \"yes\" otherwise \"no\". Also tell the correct language of the text. Your response should be JSON in the following format: {request_format}. Do not add anything else.",
		"user_prompt":       "text: {input_query}\nlanguage code: {lang_code}",
		"system_map":        map[string]string{"{request_format}": "{{\"answer\": <yes or no>, \"text_language\": <correct text language>}}"},
		"user_map":          map[string]string{"{input_query}": "早上好，今天天气很好！你觉得呢", "{lang_code}": "zh"},
		"others":            "optional input",
	}

	outputs := TensorIO{}

	err := op.Execute(inputs, outputs)
	if err != nil {
		t.Errorf("Execute returned an error: %v", err)
	}

	// TODO: Checkout Output
}
