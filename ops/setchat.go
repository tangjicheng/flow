package ops

import "flow/util"

type SetChat struct {
}

func (op *SetChat) Execute(inputs TensorIO, outputs TensorIO) error {
	var err error
	outputs["output_text"], err = util.BuildChatGPTRequestJson(inputs)
	return err
}

func (op *SetChat) GetInputsTemplate() TensorIO {
	inputs := map[string]any{
		"model":             "gpt-3.5-turbo",
		"temperature":       float32(0.0),
		"top_p":             float32(0.1),
		"frequency_penalty": float32(0.97),
		"system_prompt":     "Here is system prompt",
		"user_prompt":       "Here is user prompt",
		"system_map":        map[string]string{"{request_format}": "{{\"answer\": <yes or no>}}"},
		"user_map":          map[string]string{"{input_query}": "Hello"},
		"others":            "optional input",
	}
	return inputs
}

func (op *SetChat) GetOutputsTemplate() TensorIO {
	outputs := map[string]any{
		"output_text": "Here is the Raw ChatGPT API Request JSON Body",
	}
	return outputs
}
