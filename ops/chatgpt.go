package ops

import (
	"errors"
	"flow/util"
	"fmt"
)

type ChatGPT struct {
}

func (op *ChatGPT) Execute(inputs TensorIO, outputs TensorIO) error {
	requestBody, ok := inputs["input_text"].(string)
	if !ok {
		return errors.New("[Ops] ChatGPT inputs[input_text] must be string type")
	}

	responseBody, err := util.CallChatGPT(requestBody)
	if err != nil {
		return fmt.Errorf("[Ops] ChatGPT call chatgpt error: %s", err)
	}
	outputs["output_text"] = responseBody
	return nil
}

func (op *ChatGPT) GetInputsTemplate() TensorIO {
	inputs := map[string]any{
		"input_text": "Here is the Raw ChatGPT API Request JSON Body",
	}
	return inputs
}

func (op *ChatGPT) GetOutputsTemplate() TensorIO {
	outputs := map[string]any{
		"output_text": "Here is the Raw ChatGPT API Response JSON Body",
	}
	return outputs
}
