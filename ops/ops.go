package ops

type TensorIO map[string]any

type Ops interface {
	Execute(inputs TensorIO, outputs TensorIO) error
	GetInputsTemplate() TensorIO
	GetOutputsTemplate() TensorIO
}
