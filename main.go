package main

import (
	"flow/ops"
	"fmt"
)

func main() {
	op := &ops.SetChat{}

	inputs := ops.TensorIO{
		"model":             "gpt-3.5-turbo",
		"temperature":       float32(0.0),
		"top_p":             float32(0.1),
		"frequency_penalty": float32(0.97),
		"system_prompt":     "Check if given text matches its given language code. If they match, then your response shall be \"yes\" otherwise \"no\". Also tell the correct language of the text. Your response should be JSON in the following format: {request_format}. Do not add anything else.",
		"user_prompt":       "text: {input_query}\nlanguage code: {lang_code}",
		"system_map":        map[string]string{"{request_format}": "{{\"answer\": <yes or no>, \"text_language\": <correct text language>}}"},
		"user_map":          map[string]string{"{input_query}": "早上好，今天天气很好！你觉得呢", "{lang_code}": "zh"},
		"others":            "optional input",
	}

	outputs := ops.TensorIO{}

	err := op.Execute(inputs, outputs)
	if err != nil {
		fmt.Printf("ops.SetChat error: %s\n", err)
		return
	}

	fmt.Println(outputs["output_text"])
}
