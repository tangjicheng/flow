package util

import (
	"testing"
)

func TestCallChatGPT(t *testing.T) {
	requestBody := `{
    "model": "gpt-3.5-turbo",
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "Output a 4-word greeting"
      }
    ]
    }`

	t.Run("NormalCall", func(t *testing.T) {
		_, err := CallChatGPT(requestBody)
		if err != nil {
			t.Errorf("call chatgpt test error: %s", err)
		}
	})
}
