package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

const ChatGPTUrl = "http://gc3a.stg.g123.jp.private/v1/openai/i18n/chat/completions"

func CallChatGPT(RequestBody string) (ResponseBody string, err error) {
	reqBodyReader := bytes.NewReader([]byte(RequestBody))
	req, err := http.NewRequest("POST", ChatGPTUrl, reqBodyReader)
	if err != nil {
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	defer func() {
		if errClose := resp.Body.Close(); errClose != nil {
			err = fmt.Errorf("close error: %s, origin error: %s", errClose, err)
		}
	}()
	if err != nil {
		return
	}

	responseData, err := io.ReadAll(resp.Body)
	ResponseBody = string(responseData)
	return
}

func BuildChatGPTRequestJson(params map[string]any) (outputJson string, err error) {
	req, err := BuildChatGPTRequest(params)
	if err != nil {
		return
	}
	outputData, err := json.Marshal(req)
	outputJson = string(outputData)
	return
}

func BuildChatGPTRequest(params map[string]any) (chatReq ChatCompletionRequest, err error) {
	err = CheckChatRequestParams(params)
	if err != nil {
		return
	}
	chatReq.Model = params["model"].(string)
	chatReq.TopP = params["top_p"].(float32)
	chatReq.FrequencyPenalty = params["frequency_penalty"].(float32)

	systemPrompt := params["system_prompt"].(string)
	systemMap := params["system_map"].(map[string]string)
	systemStr := ReplaceStr(systemPrompt, systemMap)
	systemMessage := ChatCompletionMessage{
		Role:    ChatMessageRoleSystem,
		Content: systemStr,
	}
	chatReq.Messages = append(chatReq.Messages, systemMessage)

	userPrompt := params["user_prompt"].(string)
	userMap := params["user_map"].(map[string]string)
	userStr := ReplaceStr(userPrompt, userMap)
	userMessage := ChatCompletionMessage{
		Role:    ChatMessageRoleUser,
		Content: userStr,
	}
	chatReq.Messages = append(chatReq.Messages, userMessage)

	return
}

func CheckChatRequestParams(reqParams map[string]any) error {
	// TODO
	return nil
}

func ReplaceStr(originStr string, replaceMap map[string]string) string {
	if len(replaceMap) == 0 {
		return originStr
	}

	replacedStr := originStr
	for key, value := range replaceMap {
		replacedStr = strings.Replace(replacedStr, key, value, -1)
	}
	return replacedStr
}
